# -*- coding: utf-8 -*-

from lxml import html
import requests
import fnmatch
import pickle
import hashlib

class _House:
	def __init__(self, url):
		self.url = url
		self.refreshPage()

	def refreshPage(self):	
		page = requests.get(self.url)
		self.tree = html.fromstring(page.content)
		self.hash = hashlib.sha224(str(self.getHouseAddress()[0] + self.getEstateAgent())).hexdigest()

	def getPrice(self):
		self.price = self.tree.xpath('//*[@id="propertyHeaderPrice"]/strong/text()')[0].split()[0]
		return self.price

	def getPricingCondition(self): #offers from, starts at, guide price etc.
		self.pricing_condition = self.tree.xpath('//*[@id="propertyHeaderPrice"]/small/text()')
		return self.pricing_condition	

	def getEstateAgent(self):
		self.estate_agent = self.tree.xpath('//*[@id="aboutBranchLink"]/strong/text()')[0]
		return self.estate_agent

	def getFloorPlanURL(self):
		floorplan = self.tree.xpath('//*[@id="floorplansTab"]')
		if floorplan:
			self.floorplan_url =  self.tree.xpath("//*[starts-with(@id,'interactivefloorplan')]/div[2]/img[@src]")[0].attrib['src']
			return self.floorplan_url
		return False

	def getFeaturedImageURL(self):
		self.feat_img = self.tree.xpath('//*[@id="primaryContent"]/div[2]/div/div/div[1]/div[1]/div[1]/span/img[@src]')[0].attrib['src']
		return self.feat_img	

	def getHouseAddress(self):
		self.address = self.tree.xpath('//*[@id="primaryContent"]/div[1]/div/div/div[2]/div/address/text()')[0].split(', ')
		return self.address

	def getListedFeatures(self):
		self.listed_features = self.tree.xpath('//*[@id="description"]/div/div[1]/div[1]/ul/li/text()')	
		return self.listed_features

	def getContactPhone(self):
		numbers = self.tree.xpath('//*[@id="requestdetails"]/p/a/strong/text()')
		if numbers.count(numbers[0]) == len(numbers): # assumes first number will be the right one
			self.contact_numbers = numbers[0]
		else:
			self.contact_numbers = numbers
		return self.contact_numbers

	def getBedroomCount(self):
		bedrooms = self.tree.xpath("//*[contains(text(), 'bedroom')]/text()")
		room_counts_parsed = []
		for bedroom in bedrooms:
			bedroom = bedroom.split(' ')
			bedrooms_parsed = [i for i, item in enumerate(bedroom) if item.startswith('bedroom')]
			for br_p_r in bedrooms_parsed:
				try:
					room_counts_parsed.append(int(bedroom[br_p_r - 1]))
				except ValueError as e:
					pass
		self.bedroom_count = max(set(room_counts_parsed), key=room_counts_parsed.count)		
		return self.bedroom_count

	def getPricePerBedroom(self, bedrooms, price, formatted):
		if formatted:
			return "{:,}".format(int(price[2:].replace(',', '')) / bedrooms) #strip the pound sign and all the other control character crap off first
		self.price_per_bed = int(price[2:].replace(',', '')) / bedrooms	
		return self.price_per_bed

