from houses import _House
import threading

urls = [
	'http://www.rightmove.co.uk/property-for-sale/property-52811269.html',
	'http://www.rightmove.co.uk/property-for-sale/property-46900768.html',
	'http://www.rightmove.co.uk/new-homes-for-sale/property-48039688.html',
	'http://www.rightmove.co.uk/property-for-sale/property-57812759.html',
	'http://www.rightmove.co.uk/new-homes-for-sale/property-51846844.html',
	'http://www.rightmove.co.uk/property-for-sale/property-36850335.html',
	'http://www.rightmove.co.uk/property-for-sale/property-38891997.html'
]

class _House_Engine (threading.Thread):
	def __init__(self):
		print "Starting house engine..."
		self.houses = []

	def start(self, callback):
		print "Initializing..."
		for url in urls:
			self.houses.append(_House(url))
		callback()

	def getHouses(self):
		return self.houses	

	def addHouse(self, url):
		self.houses.append(_House(url))
		return True #todo: actually process house and return false if it failed