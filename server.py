from house_engine import _House_Engine
from flask import Flask, session, redirect, url_for, escape, request, render_template
from hashlib import md5
import sys
import threading
import MySQLdb

class _Server:
	global house_engine
	house_engine = _House_Engine()

	def boot(self):
		global house_engine
		house_engine.start(self.systemReady)
		
	def systemReady(self):
		print "System reports ready. Starting web service..."
		websrv = _Webserver()
		websrv.start()

class _Webserver (threading.Thread):
	app = Flask('Websrv')
	app.config['PROPAGATE_EXCEPTIONS'] = True
	app.secret_key = "Supersecretkeyssssshhhhh"


	def __init__(self):
		threading.Thread.__init__(self)

	def run(self):
		self.app.run(host='0.0.0.0', port=5000)

	@app.route('/')
	def index():
		if 'username' in session:
			global house_engine
			houses = house_engine.getHouses()
			return render_template('index.html',username=session['username'], houses=houses)
		else:
			return render_template('login.html', error="You're not logged in!")

	@app.route('/addhouse', methods=['POST'])
	def addHouse():
		house_url  = request.form['house']
		global house_engine
		if house_engine.addHouse(house_url):
			houses=house_engine.getHouses()
			return redirect('/')
		return redirect('/')

	@app.route('/login', methods=['POST'])
	def login():
		db = MySQLdb.connect(host="plantwire.co.uk", user="plantwir_wp614", passwd="O8]P-S44LA", db="plantwir_wp614")
		cur = db.cursor()
		_USER_TABLE = "wpfb_users"
		_USER_NAME_FIELD = "user_login"
		_USER_PASS_FIELD = "user_pass"

		error = None
		if 'username' in session:
			return redirect('/')
		if request.method == 'POST':
			username_form  = request.form['username']
			password_form  = request.form['password']
			cur.execute("SELECT COUNT(1) FROM " + _USER_TABLE + " WHERE " + _USER_NAME_FIELD + " = %s;", [username_form])
			if cur.fetchone()[0]:
				cur.execute("SELECT " + _USER_PASS_FIELD + " FROM " + _USER_TABLE + " WHERE " + _USER_NAME_FIELD + " = %s;", [username_form])
				for row in cur.fetchall():
					if md5(password_form).hexdigest() == row[0]:
						session['username'] = request.form['username']
						return redirect('/')
					else:
						error = "Invalid credentials"
			else:
				error = "Invalid credentials"
        	return render_template('login.html', error=error)

	@app.route('/forbidden')
	def forbidden():
		return render_template('forbidden.html')	

if __name__ == '__main__':
	print "Starting house finder utility..."
	server = _Server()
	server.boot()
	# TODO: Something useful